//
//  SearchViewController.swift
//  GithubRepoSearch
//
//  Created by Ian Houghton on 07/03/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var searchTextField : UITextField?
    @IBOutlet var searchTableView : UITableView?
    @IBOutlet var searchButton : UIButton?
    @IBOutlet var activityIndicator : UIActivityIndicatorView?
    
    var viewModel = SearchViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchTableView?.rowHeight = UITableViewAutomaticDimension
        self.searchTableView?.estimatedRowHeight = 60
        self.searchTextField?.addTarget(self, action: #selector(SearchViewController.textFieldDidChange), for: .editingChanged)
    }
    
    func textFieldDidChange(){
        self.searchButton?.isEnabled = (self.searchTextField?.text?.characters.count)! > 0
    }
    
    @IBAction func searchButtonPressed(sender: UIButton){
        self.activityIndicator?.startAnimating()
        self.searchButton?.isHidden = true
        
        self.viewModel.searchRepos(searchTerm: (self.searchTextField?.text)!) {
            self.activityIndicator?.stopAnimating()
            self.searchButton?.isHidden = false
            
            self.searchTableView?.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchToDetailSegue" {
            
            let selectedItem = self.viewModel.itemAtIndex(index: (self.searchTableView?.indexPathForSelectedRow?.row)!)
            let detailViewController = segue.destination as! DetailViewController
            detailViewController.repo = selectedItem as? GithubRepo
        }
    }
    
    // UITableViewDatasource
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.viewModel.numberOfRows()
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell =  self.searchTableView?.dequeueReusableCell(withIdentifier: "searchCell") as! SearchTableViewCell
        
        let repo = self.viewModel.itemAtIndex(index: indexPath.row)
        
        cell.configure(repo: repo as! GithubRepo)
        
        return cell
    }
    
     // UITableViewDelegate
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.performSegue(withIdentifier: "SearchToDetailSegue", sender: nil)
        tableView .deselectRow(at: indexPath, animated: false)
    }
}

