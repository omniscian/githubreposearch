//
//  DetailViewController.swift
//  GithubRepoSearch
//
//  Created by Ian Houghton on 09/03/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet var descriptionLabel : UILabel?
    @IBOutlet var numOfIssuesLabel : UILabel?
    @IBOutlet var numOfForksLabel : UILabel?
    @IBOutlet var repoTitleLabel : UILabel?
    @IBOutlet var ownerTitleLabel : UILabel?
    @IBOutlet var viewReadmeButton : UIButton?
    @IBOutlet var activityIndicator : UIActivityIndicatorView?
    
    var repo: GithubRepo?
    var viewModel = DetailViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.descriptionLabel?.text = self.repo?.repoDescription
        self.numOfForksLabel?.text = self.repo?.openForks
        self.numOfIssuesLabel?.text = self.repo?.openIssues
        self.repoTitleLabel?.text = self.repo?.name
        self.ownerTitleLabel?.text = self.repo?.ownerName
        
        self.viewReadmeButton?.isHidden = (self.repo?.readmeUrl == nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailToReadMe" {
            let destinationViewController = segue.destination as! ReadMeViewController
            destinationViewController.readMeUrl = self.viewModel.readMeUrl
        }
    }
    
    @IBAction func viewReadMeButtonPressed(){
        
        self.viewReadmeButton?.isEnabled = false
        self.activityIndicator?.startAnimating()
        
        self.viewModel.downloadReadMe(repo: self.repo!) { (resultString, hasReadMe) in
            if hasReadMe {
                self.performSegue(withIdentifier: "DetailToReadMe", sender: nil)
            }
            else{
                let alertView = UIAlertController(title: "Error", message: "No readme found", preferredStyle: UIAlertControllerStyle.alert)
                alertView.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alertView, animated: true, completion: nil)
            }
        
            self.viewReadmeButton?.isEnabled = true
            self.activityIndicator?.stopAnimating()
        }
    }
}
