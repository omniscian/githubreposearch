//
//  ReadMeViewController.swift
//  GithubRepoSearch
//
//  Created by Ian Houghton on 09/03/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class ReadMeViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet var readMeWebView : UIWebView?
    @IBOutlet var activityIndicator : UIActivityIndicatorView?
    
    var readMeUrl : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: self.readMeUrl!)
        let urlRequest = URLRequest(url: url!)
        
        self.readMeWebView?.loadRequest(urlRequest)
    }
    
    // UIWebViewDelegate
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.activityIndicator?.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator?.stopAnimating()
    }
}
