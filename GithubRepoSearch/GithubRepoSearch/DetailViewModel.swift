//
//  DetailViewModel.swift
//  GithubRepoSearch
//
//  Created by Ian Houghton on 09/03/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class DetailViewModel: NSObject {

    var readMeUrl : String?
    
    func downloadReadMe(repo : GithubRepo, completion: @escaping (_ : String?, _ : Bool) -> Void){
        
        GithubRequestManager.shared.downloadReadMe(readMeUrl: repo.readmeUrl) { (resultString, success) in
                self.readMeUrl = resultString
                completion(resultString, success)
        }

    }
}
