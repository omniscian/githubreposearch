//
//  SearchTableViewCell.swift
//  GithubRepoSearch
//
//  Created by Ian Houghton on 09/03/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet var repoLabel: UILabel?
    @IBOutlet var ownerLabel: UILabel?
    @IBOutlet var descriptionLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(repo: GithubRepo){
        self.repoLabel?.text = repo.name
        self.ownerLabel?.text = repo.ownerName
        self.descriptionLabel?.text = repo.repoDescription
    }

}
