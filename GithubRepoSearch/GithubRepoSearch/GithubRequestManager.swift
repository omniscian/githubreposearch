//
//  GithubRequestManager.swift
//  GithubRepoSearch
//
//  Created by Ian Houghton on 07/03/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

protocol GithubProviderProtocol {
    func searchGithubRepos(searchTerm: String, completion: @escaping (_ : Array<Any>?) -> Void)
}

class GithubRequestManager: NSObject, GithubProviderProtocol {

    static let shared = GithubRequestManager()
    
    private let kBaseGithubUrl = "https://api.github.com/search/repositories"
    
    func searchGithubRepos(searchTerm: String, completion: @escaping (_ : Array<Any>?) -> Void){
        
        let url = URL(string: kBaseGithubUrl + "?q=" + searchTerm.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler:{
            data, response, err -> Void in
            
            if (err == nil){
                if let parsedData = try? JSONSerialization.jsonObject(with: data!) as! [String:AnyObject] {
                    
                    //parse here
                    var results = Array<Any>()
                    
                    if let itemsArray = parsedData["items"] {
                        
                        for itemDict in itemsArray as! NSArray {
                            let repo = GithubRepo(repo: itemDict as! [String : AnyObject])
                            results.append(repo)
                        }
                    }
                    
                    DispatchQueue.main.async {
                        completion(results)
                    }
                }
            }
            else{
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        })
        
        task.resume()
        
    }
    
    func downloadReadMe(readMeUrl: String, completion: @escaping (_ : String, _ : Bool) -> Void){
        let url = URL(string: readMeUrl)
        
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler:{
            data, response, err -> Void in
            
            if (err == nil){
                if let parsedData = try? JSONSerialization.jsonObject(with: data!) as! [String:AnyObject] {
                    
                    var returnString : String?
                    var success : Bool?
                    
                    if let url = parsedData["html_url"]{
                        returnString = url as? String
                        success = true
                    }
                    else{
                        returnString = parsedData["message"] as! String?
                        success = false
                    }
                    
                    DispatchQueue.main.async {
                        completion(returnString!, success!)
                    }
                }
            }
            else{
                DispatchQueue.main.async {
                    completion("Error", false)
                }
            }
        })
        
        task.resume()
    }
}
