//
//  GithubRepoModel.swift
//  GithubRepoSearch
//
//  Created by Ian Houghton on 08/03/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

struct GithubRepo {
    let name: String
    let ownerName: String
    let url: String
    let readmeUrl: String
    let openIssues: String
    let openForks: String
    let repoDescription: String
    
    init (repo: [String: AnyObject]){
        self.name = repo["name"]! as! String
        self.ownerName = repo["owner"]!["login"] as! String
        self.url = repo["url"]! as! String
        self.readmeUrl = url + "/readme"
        let openIssuesNumber = repo["open_issues_count"]! as! NSNumber
        self.openIssues = openIssuesNumber.stringValue
        let openForksNumber = repo["forks"]! as! NSNumber
        self.openForks = openForksNumber.stringValue
        
        if let desc = repo["description"] as? String{
            self.repoDescription = desc
        }
        else
        {
            self.repoDescription = ""
        }
    }
}
