//
//  SearchViewModel.swift
//  GithubRepoSearch
//
//  Created by Ian Houghton on 08/03/2017.
//  Copyright © 2017 YakApps. All rights reserved.
//

import UIKit

class SearchViewModel: NSObject {
    
    var datasource =  Array<Any>()

    func searchRepos(searchTerm: String, completion: @escaping () -> Void){
        
        GithubRequestManager.shared.searchGithubRepos(searchTerm: searchTerm) { (result) in
            
            self.datasource = result!
            
            completion()
        }
    }
    
    func numberOfRows() -> Int {
        return (self.datasource.count)
    }
    
    func itemAtIndex(index: Int) -> Any {
        return self.datasource[index]
    }
}
